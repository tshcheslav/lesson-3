//
//  RawPeopleParse.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//

import Foundation

class RawPeopleParse {
    
    private let session = URLSession.shared

    private let baseUrl: URL = URL(string: "https://swapi.dev/api/people")!
    
    func getHero(completion: @escaping ((Result<[Hero], Error>) -> Void)) {
        let request = getRequest(url: baseUrl, method: "GET", data: nil)
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            if response.statusCode == 200 {
                if let data = data {
                    do {
                        let peopleResponses = try JSONDecoder().decode(PeopleResponses.self, from: data)
                        completion(.success(peopleResponses.results))
                    } catch {
                        completion(.failure(error))
                    }
                }
            }
        }
        .resume()
    }
    
    
    private func getRequest(url: URL, method: String, data: Data?) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpBody = data
        request.httpMethod = method

        return request
    }
}
