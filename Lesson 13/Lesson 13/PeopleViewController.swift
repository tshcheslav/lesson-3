//
//  PeopleViewController.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//

import UIKit

class PeopleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var people: [Hero] = []
    
    private let service = RawPeopleParse()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        
        service.getHero { [weak self] result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    print(error.localizedDescription)
                }
            case .success(let people):
                self?.people = people
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    

   

}
extension PeopleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellPeople", for: indexPath) as? PeopleTableViewCell else { fatalError()}
        
        cell.setup(people: people[indexPath.row])
        return cell
    }
    
    
    
    
}
