//
//  PlanetResponses.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//


import Foundation

struct PlanetResponses: Codable {
    let results: [Planet]
}
