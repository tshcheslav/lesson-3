//
//  PlanetTableViewCell.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//

import UIKit

class PlanetTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rotationLabel: UILabel!
    
    @IBOutlet weak var orbitalLabel: UILabel!
    
    
    func setup(planet: Planet) {
        nameLabel.text = planet.name
        rotationLabel.text = planet.diameter + " km"
        orbitalLabel.text = planet.orbitalPeriod + " years"
    }
}
