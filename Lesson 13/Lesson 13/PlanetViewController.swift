//
//  PlanetViewController.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//

import UIKit

class PlanetViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var planet: [Planet] = []
    
    private let service = RawPlanetParse()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        
        service.getPlanet { [weak self] result in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    print(error.localizedDescription)
                }
            case .success(let planet):
                self?.planet = planet
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    

   
}


extension PlanetViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        planet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellPlanet", for: indexPath) as? PlanetTableViewCell else {
            fatalError()}
        
        cell.setup(planet: planet[indexPath.row])
        
        return cell
    }
    
    
    
}
