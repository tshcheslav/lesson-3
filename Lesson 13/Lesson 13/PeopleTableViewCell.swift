//
//  PeopleTableViewCell.swift
//  Lesson 13
//
//  Created by 1 on 21.10.2021.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var massaLabel: UILabel!
    
    @IBOutlet weak var polLabel: UILabel!
    
    func setup(people: Hero) {
        nameLabel.text = people.name
        massaLabel.text = people.mass + " kg"
        polLabel.text = people.gender
    }
    
}
