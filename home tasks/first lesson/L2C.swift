//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L2C" //  Level name
        super.viewDidLoad()
            }
	
   
    override func run() {
        for _ in 1...8 {
		findBlock()
        moveToPeak()
        uTurn()
        moveToBottomPeak()
        }
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    
    func findBlock() {
        while frontIsClear {
            move()
        }
    }
    
    func moveToPeak() {
        turnRight()
        while leftIsBlocked {
            move()
        }
    }
    
    func uTurn() {
        turnLeft()
        move()
        turnLeft()
    }
    
    func moveToBottomPeak() {
        while frontIsClear {
            move()
        }
        turnRight()
    }
}
