//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L55H" //  Level name
        super.viewDidLoad()
            }
	
   
    override func run() {
        for _ in 1...5 {
        buildRow()
        uTurnRight()
        buildRow()
            if leftIsClear{
        uTurnLeft()
            }
        }
    }
    
    func buildRow() {
        
        while frontIsClear {
            move()
            put()
            if frontIsClear {
              move()
            }
            
            
    }
    }
    
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
        
    func uTurnRight() {
            turnRight()
            move()
            turnRight()
        }
        
    func uTurnLeft() {
            turnLeft()
            move()
            turnLeft()
        
        }
        
    
   
}

