//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        super.viewDidLoad()
            }
	
   
    override func run() {
        for _ in 1...6 {
        buildRowFirst()
            if frontIsClear {
                uTurnRight()
        buildRowSecond()
        uTurnLeft()
            }
        }
    }
    
    func buildRowFirst() {
        
        while frontIsClear {
            move()
            put()
            if frontIsClear {
              move()
            }
            
            
    }
        turnRight()
    }
    
    func buildRowSecond() {
        while frontIsClear {
            put()
            move()
            if frontIsClear {
              move()
            }
            
            
        }
    }
    
    
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
        
    func uTurnRight() {
        
            
            move()
            turnRight()

        }
        
    func uTurnLeft() {
            put()
            turnLeft()
            move()
            turnLeft()
        
        }
        
    
   
}

