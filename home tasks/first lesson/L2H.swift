//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L2H" //  Level name
        super.viewDidLoad()
            }
	
   
    override func run() {
        turnLeft()
        for _ in 1...4 {
        buildColumn()
        goToStartOfColumn()
        goToAnotherColumn()
        }
    }
    
    func buildColumn() {
        
        while frontIsClear {
            if candyPresent {
                move()
            } else {
                put()
                move()
            }
        }
    }
    
    func goToStartOfColumn() {
        put()
        turnRight()
        turnRight()
        while frontIsClear {
            move()
        }
    }
    
    func goToAnotherColumn() {
        turnLeft()
        move()
        move()
        if frontIsBlocked {
            print("That's All")
        } else {
        move()
        move()
        turnLeft()
        }
    }
    
    
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    
   
}
