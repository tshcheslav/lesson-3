//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L55H" //  Level name
        super.viewDidLoad()
            }
	
   
    override func run() {
        
        findFirstCorner()
        for _ in 1...3 {
            findCorner()
        
    }
    
    

}
    func findFirstCorner() {
        while frontIsClear {
            move()
        }
        turnRight()
        while frontIsClear {
            move()
        }
        put()
    }
    
    func findCorner() {
        turnRight()
        while frontIsClear {
            move()
        }
        if leftIsBlocked{
            put()
        }
    }
    
    func turnLeft(){
        turnRight()
        turnRight()
        turnRight()
    }
}

