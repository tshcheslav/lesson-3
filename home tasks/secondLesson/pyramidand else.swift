//
//  ViewController.swift
//  Ios1908g87lesson2
//
//  Created by 1 on 26.08.2021.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rowView(amountOfView: 3)
        pyramidView(pyramidLevel: 2)
        pyramidSecond(pyramidLevel: 4)
    }
    
    func addBox(x: Int, y: Int, height: Int = 50) {
        let boxView = UIView()
        
        boxView.frame = CGRect(x: x, y: y, width: height, height: height)
        boxView.backgroundColor = .red
        
        view.addSubview(boxView)
    }
    
    func rowView(amountOfView: Int) {
        if amountOfView <= 0 {
            return
        } else {
            var x : Int = 30
            let y : Int = 130
            let height: Int = 30
            for _ in  1...amountOfView {
                addBox(x: x, y: y, height: height)
                x = height + 10 + x
            }
        }
    }
    
    func pyramidView (pyramidLevel: Int) {
        if pyramidLevel <= 0 {
            return
        } else {
            var amountOfBoxes = 1
            var x : Int = 30
            var y : Int = 130
            
            for _ in 1...pyramidLevel {
                for _ in 1...amountOfBoxes {
                    addBox(x: x, y: y)
                    x+=60
                }
                amountOfBoxes+=1
                x = 30
                y += 60
            }
        }
    }
    
    func pyramidSecond(pyramidLevel: Int) {
        if pyramidLevel <= 0 {
            return
        } else {
            var amountOfBoxes = 1
            var x : Int = 30
            var y : Int = 130
            var startX : Int = 170
            
            for _ in 1...pyramidLevel {
                for _ in 1...amountOfBoxes {
                    addBox(x: x, y: y)
                    x+=60
                }
                amountOfBoxes+=1
                x = startX - 30
                startX = x
                y+=60
            }
            
        }
    }
}
