//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        super.viewDidLoad()
            }
	

    override func run() {
        movement(a: 11)
    }
    
    func movement(a: Int) {
        var counter = 0
        var count = 1
        for i in 0...a {
            while counter < i {
                for j in 1...i {
                    
                    put()
                    move()
                    
                    count += 1
                    
                }
                counter += 1
                
            }
               
           
            turnRight()
            turnRight()
            while frontIsClear {
                move()
            }
            turnLeft()
            move()
            turnLeft()
        }
                    }
        
        
        
    func turnLeft(){
        turnRight()
        turnRight()
        turnRight()
    }
   
    }




