//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        super.viewDidLoad()
            }
	

    override func run() {
        movement(a: 6)
    }
    func movement(a: Int) {
        for _ in 0...a {
            while frontIsClear {
                put()
                move()
            }
            put()
            turnRight()
            if leftIsBlocked {
                move()
                turnRight()
                while frontIsClear {
                    move()
                }
                turnLeft()
                if frontIsBlocked {
                    turnLeft()
                } else {
                move()
                turnLeft()
                }
            }
        }
    }
 
    
    func turnLeft(){
        turnRight()
        turnRight()
        turnRight()
    }
}

