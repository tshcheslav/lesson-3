//
//  ViewController.swift
//  Lesson 4
//
//  Created by 1 on 02.09.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        printNameChatactersCount()
        hasEnding()
        seperator(fullname: "IvanVasilevich")
        reverse("Omnomnom")
        addSeparator()
        checkPassword(password: "3=+45ghj@ghLLgj")
        sortedd()
        serching()
        
    }
    
    func printNameChatactersCount() {
        let name = "Nikita Babushkin"
        print(name.count)
    }
    
    func hasEnding() {
        let name = ["Евгениевич"]
        for ending in name {
            if ending.hasSuffix("ич") {
                print("Have ич")
            } else if ending.hasPrefix("на") {
                print("Have на")
            } else {
                print("No russian ending")
            }
        }
    }
    
    func seperator(fullname: String) {
        var bigLetter = ""
        for i in fullname.reversed() {
            if i.isUppercase {
                bigLetter = "\(i)"
                break
            }
        }
        let splitted = fullname.split(separator: Character(bigLetter))
        if let firstName = splitted.first, let surname = splitted.last {
            print(firstName)
            print("\(bigLetter + surname)")
            print("\(firstName) + \(bigLetter + surname)")
        }
        
    }
    
    func reverse(_ str: String)  {
        
        var str = str
        
        var indexLeft = 0
        var indexRight = str.count - 1
        
        while indexLeft < indexRight {
            let a = str[str.index(str.startIndex, offsetBy: indexLeft)]
            let b = str[str.index(str.startIndex, offsetBy: indexRight)]
            
            str = String(str.prefix(indexLeft) + String(b) + str.suffix(from: str.index(str.startIndex, offsetBy: indexLeft + 1)))
            str = String(str.prefix(indexRight) + String(a) + str.suffix(from: str.index(str.startIndex, offsetBy: indexRight + 1)))
            indexLeft += 1
            indexRight -= 1
        }
        
        print(str)
        
    }
    
    func addSeparator() {
        var num = "1223678"
        if num.count % 2 == 0 {
            num.insert(",", at: num.index(num.startIndex, offsetBy: 1))
        } else if num.count % 5 == 0 {
            num.insert(",", at: num.index(num.startIndex, offsetBy: 2))
        } else if num.count % 7 == 0 {
            num.insert(",", at: num.index(num.startIndex, offsetBy: 1))
            num.insert(",", at: num.index(num.startIndex, offsetBy: 5))
        }
        
        print(num)
    }
    
    func checkPassword(password: String) {
        var level: [String] = []
       for character in password {
            if character.isNumber {
                level.append("a")
            }  else if character.isUppercase {
                level.append("b")
            } else if character.isLowercase {
                level.append("c")
            } else if character.isSymbol {
                level.append("d")
            }
        }
        
        
        print(Set(level))
        
    }
    func sortedd () {
        let arr = [9, 1, 2, 5, 1, 7]
        let unique = Set(arr)
        let sort = unique.sorted { first, next in
            return first < next
        }
        print(sort)
    }
    func serchingDa() {
        let words = ["alda", "dagestan", "dangerous", "cfdjgjuhk"]
        for i in words {
            if i.hasSuffix("da") || i.hasPrefix("da") {
                print(i)
            }
        }
    }
    
    func serching() {
        let words = ["fuck", "fucking", "fucks", "fuckfuck", "no"]
        var changes = ""
        for i in words {
            if i.hasSuffix("uck") || i.hasPrefix("uck") {
                changes = i.replacingOccurrences(of: "uck", with: "***")
            }
            print(changes)
        }
    }
    
    func transliterate(nonLatin: String) -> String {
    }
}

