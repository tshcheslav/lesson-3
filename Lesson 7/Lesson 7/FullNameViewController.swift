//
//  FullNameViewController.swift
//  Lesson 7
//
//  Created by 1 on 23.09.2021.
//

import UIKit

class FullNameViewController: UIViewController {
    
    @IBOutlet weak var nameLabelTwo: UILabel!
    @IBOutlet weak var nameLabelThree: UILabel!
    @IBOutlet weak var surnameLabelTwo: UILabel!
    @IBOutlet weak var surnameLabelThree: UILabel!
    
    var name: String?
    var surname: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let name = self.name else {return}
        guard let surname = self.surname else {return}
        nameLabelThree.text = "\(name)"
        surnameLabelThree.text = "\(surname)"
    }
    

    
    @IBAction func saveAction(_ sender: UIButton) {
        performSegue(withIdentifier: "unwindSegue", sender: nil)
    }
    
    
}
