//
//  NameViewController.swift
//  Lesson 7
//
//  Created by 1 on 23.09.2021.
//

import UIKit



class NameViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullNameLabel.isHidden = true
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let tvc = segue.destination as? SurnameViewController else {return}
       tvc.resultName = nameTF.text
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func unwindSegueToMainScreen(segue: UIStoryboardSegue) {
        guard segue.identifier == "unwindSegue" else {return}
        guard let svc = segue.source as? FullNameViewController else {return}
        fullNameLabel.isHidden = false
        self.fullNameLabel.text = "\(svc.nameLabelThree.text!)  \(svc.surnameLabelThree.text!)"
    }

    
    @IBAction func nextActionOne(_ sender: UIButton) {
        performSegue(withIdentifier: "surnameSegue", sender: nil)
    }
    

}
