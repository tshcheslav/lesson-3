//
//  SurnameViewController.swift
//  Lesson 7
//
//  Created by 1 on 23.09.2021.
//

import UIKit



class SurnameViewController: UIViewController {

    @IBOutlet weak var surnameLabel: UILabel!
    
    @IBOutlet weak var surnameTF: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    var resultName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultLabel.isHidden = true
        guard let resultName = self.resultName else {return}
        resultLabel.text = "\(resultName)"
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let tfvc = segue.destination as? FullNameViewController else {return}
        tfvc.surname = surnameTF.text
        tfvc.name = resultLabel.text
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
  
    @IBAction func surnameAction(_ sender: UIButton) {
        performSegue(withIdentifier: "detailSegue", sender: nil)
    }
    
}
