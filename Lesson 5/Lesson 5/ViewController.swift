//
//  ViewController.swift
//  Lesson 5
//
//  Created by 1 on 14.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var actionLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionLabel.text = model.consoleText
        
        
    }
    
    let model = CoffeMachine(waterSection: 100.0, coffeSection: 50.0, milkSection: 40.0, trashBin: 0.0, consoleText: "Choose your coffee")
    
    @IBAction func makeAmericanoAction(_ sender: UIButton) {
        model.makeA()
        actionLabel.text = model.consoleText
    }
    
    @IBAction func makeLatteAction(_ sender: UIButton) {
        model.makeL()
        actionLabel.text = model.consoleText
    }
    
    @IBAction func clearButton(_ sender: UIButton) {
        model.cleanTrashBin()
        actionLabel.text = model.consoleText
    }
    @IBAction func addMilkButton(_ sender: UIButton) {
        model.addMilk()
        actionLabel.text = model.consoleText
    }
    
    @IBAction func addWaterButton(_ sender: UIButton) {
        model.addWater()
        actionLabel.text = model.consoleText
    }
    @IBAction func addBeansButton(_ sender: UIButton) {
        model.addBeens()
        actionLabel.text = model.consoleText
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        model.textClear()
        actionLabel.text = model.consoleText
    }
}

