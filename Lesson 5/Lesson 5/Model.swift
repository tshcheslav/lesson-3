//
//  Model.swift
//  Lesson 5
//
//  Created by 1 on 14.09.2021.
//

import Foundation

class CoffeMachine {
    var waterSection: Double
    var coffeSection: Double
    var milkSection: Double
    var trashBin: Double
    var consoleText: String
    
    init(waterSection: Double, coffeSection: Double, milkSection: Double, trashBin: Double, consoleText: String ) {
        self.waterSection = waterSection
        self.coffeSection = coffeSection
        self.milkSection = milkSection
        self.trashBin = trashBin
        self.consoleText = consoleText
    }
    /*
    func makeLatte() {
        if trashBin < 1500.0 , milkSection > 50.0 , coffeSection > 10.0 , waterSection > 25.0 {
            consoleText = "Your Latte"
            waterSection -= 24.0
            coffeSection -= 9.0
            milkSection -= 49.0
            trashBin += 250.0
        } else if trashBin < 1500.0 , milkSection > 50.0 , coffeSection > 10.0 , waterSection < 25.0 {
            consoleText = "no enough water"
        } else if trashBin < 1500.0 , milkSection > 50.0 , coffeSection < 10.0 , waterSection > 25.0 {
            consoleText =  "no enough beens"
        } else if trashBin < 1500.0 , milkSection < 50.0 , coffeSection > 10.0 , waterSection > 25.0 {
            consoleText = "no enough milk"
        } else if trashBin > 1500.0 , milkSection > 50.0 , coffeSection > 10.0 , waterSection > 25.0 {
            consoleText = "no enough place"
        } else {
            while trashBin < 1500.0 , milkSection > 50.0 , coffeSection > 10.0 , waterSection > 25.0 {
                addWater()
                addMilk()
                addBeens()
                cleanTrashBin()
            }
        }
        
        
    }
    
    func makeAmericano() {
        if trashBin < 1500.0 ,  coffeSection > 10.0 , waterSection > 25.0 {
            consoleText = "Your Amerricano"
            waterSection -= 24.0
            coffeSection -= 9.0
            trashBin += 250.0
        } else if trashBin < 1500.0 ,  coffeSection > 10.0 , waterSection < 25.0 {
            consoleText = "no enough water"
        } else if trashBin < 1500.0 ,  coffeSection < 10.0 , waterSection > 25.0 {
            consoleText = "no enough beens"
        } else if trashBin > 1500.0 ,  coffeSection > 10.0 , waterSection > 25.0 {
            consoleText = "no enough place"
        } else {
            while trashBin < 1500.0 , coffeSection > 10.0 , waterSection > 25.0 {
                addWater()
                addBeens()
                cleanTrashBin()
            }
        }
    }
     */
    
    func makeA() {
        guard trashBin < 1500.0 else {
            consoleText = "no enough place"
            return }
        guard coffeSection > 10.0  else {
            consoleText = "no enough beens"
            return
        }
        guard waterSection > 25.0 else {
            consoleText = "no enough water"
            return
        }
        consoleText = "Your Amerricano"
        waterSection -= 24.0
        coffeSection -= 9.0
        trashBin += 250.0
    }
    
    
    func makeL() {
        guard trashBin < 1500.0 else {
            consoleText = "no enough place"
            return }
        guard coffeSection > 10.0  else {
            consoleText = "no enough beens"
            return
        }
        guard waterSection > 25.0 else {
            consoleText = "no enough water"
            return
        }
        guard milkSection > 50.0  else {
            consoleText = "no enough milk"
            return
        }
        consoleText = "Your Latte"
        waterSection -= 24.0
        coffeSection -= 9.0
        milkSection -= 49.0
        trashBin += 250.0
    }
    
        func addWater() {
            waterSection += 300.0
            consoleText = "water added"
        }
        
        func addMilk() {
            milkSection += 100.0
            consoleText = "milk added"
        }
        
        func addBeens() {
            coffeSection += 100.0
            consoleText = "beans added"
        }
        
        func cleanTrashBin() {
            trashBin -= 500
            consoleText = "trash bin is clear"
        }
        
        func textClear() {
            consoleText = ""
        }
        
    }

