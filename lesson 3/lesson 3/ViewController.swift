//
//  ViewController.swift
//  lesson 3
//
//  Created by 1 on 02.09.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        findHighestNumber(a: 4, b: 8)
        multipleA(c: 2)
        printNumber(d: 10)
        printNumberTwo(d: 12)
        find(f: 8)
        findUnicNumber(inputNumber: 6)
        taskOne()
        howMuch()
        howMuchMounth()
        reverse("1234567")
    }
    
    func findHighestNumber (a:Int , b: Int )  {
        if a > b {
            print(a)
        } else if b > a {
            print(b)
        }
    }
    
    func multipleA (c:Int) {
        print(c * c)
        print( c * c * c)
    }

    func printNumber (d: Int) {
        var i = 0
        while i < d {
            print(i)
            i += 1
        }
       
    }
    func printNumberTwo (d: Int) {
        var i = d
        while i > 0  {
            print(i)
            i -= 1
        }
    }
    
    func find (f: Int) {
        for i in 1...f {
            if f % i == 0 {
                print(i)
            }
            
        }
    }
    
    func findUnicNumber (inputNumber: Int) {
            var sum = 0
        
        for i in 1...inputNumber - 1 {
            if inputNumber % i == 0 {
                sum += i
            }
        }
        if sum == inputNumber {
            print("\(inputNumber) is perfect number")
        } else {
            print("\(inputNumber) is not perfect number")
        }
        
    }
    
    func taskOne () {
        let timeDiference = 195
        var sum = Int(24.0)
        let yaerPlus = Int(1.44)
        for _ in 1...timeDiference {
            sum += yaerPlus
        }
        print(sum)
    }
    
    
    func howMuch () {
        
        var sum = 1000.0
        let percent = 1.03
        var percentTwo = 1.03
        var tenMonth = 9000
        var whatWeNeed = 0
        var whatHave = 7000
        var whatHavent = 0
        for _ in 1...9 {
            percentTwo *= percent
            
    }
        whatWeNeed = Int(percentTwo * sum) + 9000
        whatHavent = Int(whatWeNeed) - 7000
        print(whatHavent)
}
    
    func howMuchMounth() {
        
        var sum = 1000.0
        let percent = 1.03
        let grant = 700
        var whatHave = 2400
        var sumSpend = Int(sum * percent)
        var monthLeave = 0
        while whatHave > 0 {
            whatHave += grant - Int(sumSpend)
            monthLeave += 1
        }
        print(monthLeave - 1)
    }
     

    

    func reverse(_ str: String)  {
        
        var str = str
        
        var indexLeft = 0
        var indexRight = str.count - 1
        
        while indexLeft < indexRight {
            let a = str[str.index(str.startIndex, offsetBy: indexLeft)]
            let b = str[str.index(str.startIndex, offsetBy: indexRight)]
            
            str = String(str.prefix(indexLeft) + String(b) + str.suffix(from: str.index(str.startIndex, offsetBy: indexLeft + 1)))
            str = String(str.prefix(indexRight) + String(a) + str.suffix(from: str.index(str.startIndex, offsetBy: indexRight + 1)))
            indexLeft += 1
            indexRight -= 1
        }
        
        print(str)
        
    }

}

