//
//  ViewController.swift
//  Lesson 8
//
//  Created by 1 on 30.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.frame = CGRect(x: 25.0, y: 25.0, width: 240.0, height: 128.0)
        imageView.backgroundColor = .blue
    }
    @IBAction func tapGersureAction(_ sender: UITapGestureRecognizer) {
        updatePosition()
    }
    
    @objc func updatePosition() {
        let maxX = view.frame.maxX - imageView.frame.width
        let maxY = view.frame.maxY - imageView.frame.height
        let xCoord = CGFloat.random(in: 0...maxX)
        let yCoord = CGFloat.random(in: 0...maxY)

        UIView.animate(withDuration: 0.3) { [self] in
            self.imageView.transform = CGAffineTransform(translationX: xCoord, y: yCoord)
        }
    }
}

