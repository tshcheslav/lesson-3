//
//  DarkTheme.swift
//  Weather Application
//
//  Created by 1 on 02.11.2021.
//

import UIKit

class DarkTheme: ThemeProtocol {
    
    var backgroundColor: UIColor = UIColor(named: "backgroundDark")!
    
    var textColor: UIColor = UIColor(named: "textColorLight")!
    
    var accentColor: UIColor = UIColor(named: "accentColorLight")!
    
}
