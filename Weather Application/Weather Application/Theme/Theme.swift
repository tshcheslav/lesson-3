//
//  Theme.swift
//  Weather Application
//
//  Created by 1 on 02.11.2021.
//

import UIKit

protocol ThemeProtocol {
    var backgroundColor: UIColor {get}
    var textColor: UIColor {get}
    var accentColor: UIColor {get}
    
}

class Theme {
    static var currentTheme :ThemeProtocol = LightTheme()
}
