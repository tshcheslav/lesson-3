//
//  LightTheme.swift
//  Weather Application
//
//  Created by 1 on 02.11.2021.
//

import UIKit

class LightTheme: ThemeProtocol {
    var backgroundColor: UIColor = UIColor(named: "backgroundLight")!
    
    var textColor: UIColor = UIColor(named: "textColorDark")!
    
    var accentColor: UIColor = UIColor(named: "accentColorDark")!
    
}
