//
//  SettingsViewController.swift
//  Weather Application
//
//  Created by 1 on 02.11.2021.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var switcher: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func changeTheme(_ sender: UISwitch) {
        Theme.currentTheme = sender.isOn ? DarkTheme() : LightTheme()
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        self.switcher.onTintColor = Theme.currentTheme.accentColor
        self.textLabel.textColor = Theme.currentTheme.textColor
    }
    

}
