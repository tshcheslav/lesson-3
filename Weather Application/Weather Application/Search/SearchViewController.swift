//
//  SearchViewController.swift
//  Weather Application
//
//  Created by 1 on 01.11.2021.
//

import UIKit

class SearchViewController: UIViewController, UISearchResultsUpdating  {

    @IBOutlet weak var tableView: UITableView!
    
    var timer =  Timer()
    
    var cityName: String?
    
    var dateIsReady:Bool = false
    var offerModel:OfferModel! {
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
    }
    
    
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let city = searchController.searchBar.text!
        cityName = city
        timer.invalidate()
        
        if city != "" {
            timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (timer) in
                NetworkManager.shared.getWeather(city: city, result: { (model) in
                    guard let model = model,
                          let list = model.list else {return}
                        self.dateIsReady = true
                  //      let range = model!.list!.prefix(1)
                    model.list = list.filter({ $0.dt_txt!.contains("12:00:00") == true})
                        self.offerModel = model
                    
                })
            })
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go" {
            let indexPath = self.tableView.indexPathForSelectedRow
            guard let vc = segue.destination as? CurrentViewController else {return}
            let date = self.offerModel.list![indexPath!.row].dt_txt!
            let iconName = self.offerModel.list![indexPath!.row].weather![0].icon
            vc.times = String(date.prefix(10))
            vc.dates = String(date.suffix(9))
            vc.tenps = "\(Int(round((self.offerModel.list![indexPath!.row].main!.temp!) - 273.15)))" + " ℃"
            vc.citys = self.cityName!
            vc.iconName = iconName
        }
    }
    
    
    
}

extension SearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dateIsReady {
            return self.offerModel!.list!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? WeatherTableViewCell else { fatalError() }
        let iconName = self.offerModel.list![indexPath.row].weather![0].icon
        let date = self.offerModel.list![indexPath.row].dt_txt!
        cell.dateLabel.text = String(date.suffix(9))
        cell.timeLabel.text = String(date.prefix(10))
        cell.tempLabel.text = "\(Int(round((self.offerModel.list![indexPath.row].main!.temp!) - 273.15)))" + " ℃"
        cell.iconLabel.image = UIImage(named: iconName!)
        
        
        
//        let suffix = iconName?.suffix(1)
//        if suffix == "n" {
//            cell.backgroundColor = .gray
//        } else {
//            cell.backgroundColor = .blue
//        }
        
        
        return cell
        
        
        
        
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "go", sender: nil)
//        dismiss(animated: true, completion: nil)
    }
}
