//
//  LocationViewController.swift
//  Weather Application
//
//  Created by 1 on 01.11.2021.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CoreLocation

class LocationViewController: UIViewController, CLLocationManagerDelegate {
    
    let defaults = UserDefaults.standard
    
    var activityIndicator: NVActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    
    let gradientLayer = CAGradientLayer()
    
    let apiKey = "0f892aa904a9a5cbfa7de59338ef4eba"
    var lat = 11.344533
    var lon = 104.33322
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var conditionImageView: UIImageView!
    
    @IBOutlet weak var conditionLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var backgroundView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        backgroundView.layer.addSublayer(gradientLayer)
        
        
        
        
        
        
        let indicatorSize: CGFloat = 70
        let indicatorFrame = CGRect(x: (view.frame.width - indicatorSize)/2,
                                    y: (view.frame.height - indicatorSize)/2,
                                    width: indicatorSize,
                                    height: indicatorSize)
        
        activityIndicator = NVActivityIndicatorView(frame: indicatorFrame, type: .lineScale, color: UIColor.white, padding: 20.0)
        activityIndicator.backgroundColor = UIColor.black
        view.addSubview(activityIndicator)
        
        
        locationManager.requestWhenInUseAuthorization()
        
        activityIndicator.startAnimating()
        if (CLLocationManager.locationServicesEnabled()){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
        
        //MARK: Checking of connection type
        if Reachability.isConnectedToNetwork() == false {
//            if defaults.accessibilityTraits.isEmpty {
            activityIndicator.stopAnimating()
            presenter()
//            showAlert()
//        }
        
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        setBlueGradientBackground()
//    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        lat = location.coordinate.latitude
        lon = location.coordinate.longitude
        Alamofire.request("http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(apiKey)&units=metric").responseJSON { [self] response in
            
            self.activityIndicator.stopAnimating()
            if let responseStr = response.result.value {
                let jsonResponse = JSON(responseStr)
                let jsonWeather = jsonResponse["weather"].array![0]
                let jsonTemp = jsonResponse["main"]
                let iconName = jsonWeather["icon"].stringValue
                
                
                self.locationLabel.text = jsonResponse["name"].stringValue
                self.conditionImageView.image = UIImage(named: iconName)
                self.conditionLabel.text = jsonWeather["main"].stringValue
                self.temperatureLabel.text = "\(Int(round(jsonTemp["temp"].doubleValue)))"
                
                
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE"
                self.dayLabel.text = dateFormatter.string(from: date)
                
                let suffix = iconName.suffix(1)
                if suffix == "n" {
                    self.setGrayGradientBackground()
                } else {
                    self.setBlueGradientBackground()
                }
                
                //MARK: UserDefaultsSetandSave
                
                let city = self.locationLabel.text
                let condition = self.conditionLabel.text
                let temp = self.temperatureLabel.text
                let day = self.dayLabel.text
                let image = iconName
                
                
                defaults.set(city, forKey: "city")
                defaults.set(condition, forKey: "condition")
                defaults.set(temp, forKey: "temp")
                defaults.set(day, forKey: "day")
                defaults.set(image, forKey: "image")
            }
        }
        
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    func presenter() {
        guard let images = defaults.string(forKey: "image") else {return}
        
        locationLabel.text = defaults.string(forKey: "city")
        conditionLabel.text = defaults.string(forKey: "condition")
        temperatureLabel.text = defaults.string(forKey: "temp")
        dayLabel.text = defaults.string(forKey: "day")
        conditionImageView.image = UIImage(named: images)
        if images.suffix(1) == "n" {
            self.setGrayGradientBackground()
        } else {
            self.setBlueGradientBackground()
        }
    }
    
    
    
    
    // MARK: Error
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    // MARK: Gradient
    func setBlueGradientBackground() {
        let topColor = UIColor(red: 95.0/255.0, green: 165.0/255.0, blue: 1.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
        
    }
    
    func setGrayGradientBackground() {
        let topColor = UIColor(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor]
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "No Conection", message: "Faild Internetconection", preferredStyle: .alert)
        let showMessageAction = UIAlertAction(title: "ok", style: .cancel, handler: nil)
        alert.addAction(showMessageAction)
        present(alert, animated: true, completion: nil)
    }
    
}
