//
//  CurrentViewController.swift
//  Weather Application
//
//  Created by 1 on 01.11.2021.
//

import UIKit

class CurrentViewController: UIViewController {
    var times: String?
    var citys: String?
    var tenps: String?
    var dates: String?
    var iconName: String?
    
    
    @IBOutlet weak var weatherLabel: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var tenp: UILabel!
    @IBOutlet weak var date: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.city.text = citys
        self.date.text = dates
        self.tenp.text = tenps
        self.time.text = times
        self.weatherLabel.image = UIImage(named: iconName!)
        
        self.view.backgroundColor = Theme.currentTheme.backgroundColor
        time.textColor = Theme.currentTheme.textColor
        city.textColor = Theme.currentTheme.textColor
        tenp.textColor = Theme.currentTheme.textColor
        date.textColor = Theme.currentTheme.textColor
    }
    

   

}
