//
//  WeatherTableViewCell.swift
//  Weather Application
//
//  Created by 1 on 01.11.2021.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var iconLabel: UIImageView!
    
    
}
