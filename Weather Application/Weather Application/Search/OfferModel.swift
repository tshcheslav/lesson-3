//
//  OfferModel.swift
//  Weather Application
//
//  Created by 1 on 01.11.2021.
//

import Foundation

class OfferModel: Codable {
    var list: [ListOfferModel]?
    var city: CityModel?
}

class CityModel: Codable {
    var name: String?
    var country: String?
}

class ListOfferModel: Codable {
    var main: MainOfferModel?
    var dt_txt: String?
    var weather: [WeatherOfferModel]?
}

class WeatherOfferModel: Codable {
    var description: String?
    var icon: String?
}

class MainOfferModel: Codable {
    var temp: Float?
    var temp_min: Float?
    var temp_max: Float?
}

