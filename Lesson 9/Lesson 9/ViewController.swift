//
//  ViewController.swift
//  Lesson 9
//
//  Created by 1 on 01.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var newDev: [DevelopersStruct] = []
    
    var developers: [DevelopersStruct] =
    [
        DevelopersStruct(name: "Nikita", nickName: "a1"),
        DevelopersStruct(name: "Artem Velikiy", nickName: "bazinga"),
        DevelopersStruct(name: "Anton", nickName: "intern"),
        DevelopersStruct(name: "Viktor", nickName: "vikor"),
        DevelopersStruct(name: "Valeriy", nickName: "valeriy"),
        DevelopersStruct(name: "Valentina", nickName: "valentina"),
        DevelopersStruct(name: "Sergey", nickName: "sergey"),
        DevelopersStruct(name: "Perto", nickName: "p.starychok"),
        DevelopersStruct(name: "Evgeniy", nickName: "evgeniy"),
        DevelopersStruct(name: "Dima", nickName: "dima"),
        DevelopersStruct(name: "Blackice", nickName: "blackice"),
        DevelopersStruct(name: "Artem Zosyuk", nickName: "a.zosyuk"),
        DevelopersStruct(name: "Artem Bilous", nickName: "a.bilous")
        
        
    ]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if sortButton.title == "Sort by nickname" {
            newDev = developers.sorted( by: { $0.nickName < $1.nickName } )
        } else {
            newDev = developers.sorted( by: { $0.name < $1.name } )
        }
        self.title = "Hello \(newDev.count) participants"
        sortButton.title = "Sort by nickname"
        

    }
    
    
    
    @IBAction func sortBy(_ sender: UIBarButtonItem) {
        if sortButton.title == "Sort by nickname" {
            newDev = developers.sorted( by: { $0.nickName < $1.nickName } )
            sortButton.title = "Sort by name"
            tableView.reloadData()
            
        } else {
            newDev = developers.sorted( by: { $0.name < $1.name } )
            sortButton.title = "Sort by nickname"
            tableView.reloadData()
        }
    }
    
}



extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return developers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TableViewCell else {fatalError("Error")}
        cell.nameLabel.text =  newDev[indexPath.row].name
        cell.nickNameLabel.text =  newDev[indexPath.row].nickName
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .gray
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section Header for names and nicknames"
    }
    
    
    
}

